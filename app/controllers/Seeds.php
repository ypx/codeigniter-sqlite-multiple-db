<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seeds extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->faker = Faker\Factory::create('id_ID');
        $this->peopledb = $this->load->database('people', TRUE);
        $this->newsdb = $this->load->database('news', TRUE);
    }

    public function index()
    {
        
    }

    public function people( $count = 100 )
    {
        for($i = 0; $i < $count; $i++) {
            // $data['id'] = '';
            $data['name'] = $this->faker->name;
            $data['email'] = $this->faker->email;
            $this->peopledb->insert('users', $data);
        }
    }

    public function news( $count = 100 )
    {
        for($i = 0; $i < $count; $i++) {
            // $data['id'] = '';
            $data['title'] = $this->faker->sentence($nbWords = rand(3,9), $variableNbWords = true);
            $data['content'] = $this->faker->realText($maxNbChars = 200, $indexSize = 2);
            $this->newsdb->insert('articles', $data);
        }
    }

}