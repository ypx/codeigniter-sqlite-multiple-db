<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->mod = 'data';
        $this->load->model('M_data', 'data');
    }

    public function index()
    {
        redirect( $this->mod . '/view');
    }

    public function view()
    {
        $data['people'] = $this->data->GetPeople();
        $data['news'] = $this->data->GetNews();
        $this->load->view('v_data', $data, FALSE);
    }

}