<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
        $this->peopledb = $this->load->database('people', TRUE);
        $this->newsdb = $this->load->database('news', TRUE);
    }

    public function GetPeople()
    {
        return $this->peopledb->get('users');
    }

    public function GetNews()
    {
        return $this->newsdb->get('articles');
    }


}