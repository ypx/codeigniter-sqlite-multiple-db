<html>
    <head>
        <title>SQLite Test</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    </head>
    <body>
        <div class="row">
            <div class="container">
                <h1>People Data</h1>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($people->result() as $person): ?>
                        <tr>
                            <td><?php echo $person->name ?></td>
                            <td><?php echo $person->email ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <h1>News Data</h1>
                <table class="table table-bordered datatable">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Content</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($news->result() as $person): ?>
                        <tr>
                            <td><?php echo $person->title ?></td>
                            <td><?php echo $person->content ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.datatable').DataTable();
            });
        </script>
    </body>
</html>